
package lab7;


public class Lab7 {


    public static void main(String[] args) {
        Numeral e3 = new Numeral(3);
        Square e32 = new Square(e3);
        Addition e321_add = new Addition(e32, new Numeral(1));
        Multiplication e321_multi = new Multiplication(e32, new Numeral(1));
        Subtraction e321_sub = new Subtraction(e32, new Numeral(1));
        Division e321_div = new Division(e32, new Numeral(1));

        Expression eAdd = new Square(e321_add);
        Expression eMulti = new Square(e321_multi);
        Expression eSub = new Square(e321_sub);
        Expression eDiv = new Square(e321_div);

        System.out.println("ADD: "+eAdd + " = " + eAdd.evaluate());
        System.out.println("MULTI: "+eMulti + " = " + eMulti.evaluate());
        System.out.println("SUB: "+ eSub + " = " + eSub.evaluate());
        System.out.println("DIV: "+ eDiv + " = " + eDiv.evaluate());
    }

}

class Expression {

    public String toString() {
        return null;
    }
	public int evaluate() {
        return 0;
    }
}

class Numeral extends Expression {

    int value;

    public Numeral(int n) {
        value = n;
    }

    public String toString() {
        return value + "";
    }

    public int evaluate() {
        return value;
    }
}

class Square extends Expression {

    private Expression expression;

    public Square(Expression e) {
        expression = e;
    }

    public String toString() {
        return expression.toString() + "^2";
    }

    public int evaluate() {
        int r = expression.evaluate();
        return r * r;
    }
}

class BinaryExpression extends Expression {

    public Expression left() {
        return null;
    }

    public Expression right() {
        return null;
    }
}

class Addition extends BinaryExpression {

    private Expression left;
    private Expression right;

    public Addition(Expression left, Expression right) {
        this.left = left;
        this.right = right;
    }

    public Expression left() {
        return left;
    }

    public Expression right() {
        return right;
    }

    public String toString() {
        return "(" + left.toString()
                + "+" + right.toString() + ")";
    }

    public int evaluate() {
        return left.evaluate() + right.evaluate();
    }
}

class Subtraction extends BinaryExpression {

    private Expression left;
    private Expression right;

    public Subtraction(Expression left, Expression right) {
        this.left = left;
        this.right = right;
    }

    public Expression left() {
        return left;
    }

    public Expression right() {
        return right;
    }

    public String toString() {
        return "(" + left.toString()
                + "-" + right.toString() + ")";
    }

    public int evaluate() {
        return left.evaluate() - right.evaluate();
    }
}

class Multiplication extends BinaryExpression {

    private Expression left;
    private Expression right;

    public Multiplication(Expression left, Expression right) {
        this.left = left;
        this.right = right;
    }

    public Expression left() {
        return left;
    }

    public Expression right() {
        return right;
    }

    public String toString() {
        return "(" + left.toString()
                + "*" + right.toString() + ")";
    }

    public int evaluate() {
        return left.evaluate() * right.evaluate();
    }
}

class Division extends BinaryExpression {

    private Expression left;
    private Expression right;

    public Division(Expression left, Expression right) {
        this.left = left;
        this.right = right;
    }

    public Expression left() {
        return left;
    }

    public Expression right() {
        return right;
    }

    public String toString() {
        return "(" + left.toString()
                + "/" + right.toString() + ")";
    }

    public int evaluate() {
        return left.evaluate() / right.evaluate();
    }
}
