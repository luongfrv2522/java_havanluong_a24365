import java.util.Scanner;
public class functionBT {
    public int findUCLN(int a,int b){

        if(a<=0 || b<=0){
            System.out.println("Khong xet! a or b <=0");
            return 0;
        }
        
        if(a<b){
            int c=a;
            a=b;
            b=c;
        }
        
        for(int i=b; i>=1;i--){
            if(a%i==0&&b%i==0){
                return i;
            }
        }
        return 1;
    }
    
    private int fibo(int s){
           
    if(s==0||s==1) return 1;
    else
            return (fibo(s-2)+fibo(s-1));
    }
    
    public void fibonaci(int n){
        if(n>0){
            System.out.println("Day fibo: ");
            int tong=0;
            for (int i = 0; i < n; i++) {
                tong+=fibo(i);
                System.out.print(fibo(i)+" ");
            }
            //System.out.println("\nTong fibo: "+tong);
        }
        else{
            System.out.println("Khong xet n<=0");
        }
    }
}