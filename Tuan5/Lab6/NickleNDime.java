
package Tuan5.Lab6;


public class NickleNDime extends Account{
    
    private int withdrawCount;

    public NickleNDime() {
        withdrawCount=0;
    }

    @Override
    public boolean withdraw(float amount) {
        boolean flag=super.withdraw(amount);
        if(flag)
            withdrawCount++;
        return flag;
    }
    
    @Override
    public float endMonthCharge() {
        return (float) (0.5 * withdrawCount);
    }
    
}
