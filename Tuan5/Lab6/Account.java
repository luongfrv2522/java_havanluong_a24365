/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tuan5.Lab6;

/**
 *
 * @author Admin
 */
public abstract class Account {
    protected float balance;
    protected int transactions;

    public Account() {
        balance=0;
    }

    public Account(float balance) {
        this.balance = balance;
    }

    public int getTransactions() {
        return transactions;
    }

    public float getBalance() {
        return balance;
    }
    
    public void deposit(float amount){
        balance+=amount;
        transactions++;
    }
    public boolean withdraw(float amount){
        if(balance<=amount||balance<=0){
            System.out.println("Khong the rut qua so tien trong tai khoan.");
            return false;
        }
        if(balance<=endMonthCharge()){
            System.out.println("Khong the rut qua so tien dich vu phai tra.");
            return false;
        } 
        balance-=amount;
        transactions++;
        return true;
        
    }
    public void endMonth(){
        balance-=endMonthCharge();
        System.out.println("So du TK: "+balance);
        System.out.println("So giao dich trong thang: "+transactions);
        transactions=0;
    }
    
    abstract public float endMonthCharge();
    
}
