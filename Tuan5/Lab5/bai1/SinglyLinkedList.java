package Tuan5.Lab5.bai1;

class Node {

    String Content;
    Node Next;
    Node Prev;

    public Node() {
        Next = null;
        Prev = null;
        Content = "";
    }

    public Node(String Content) {
        this.Content = Content;
    }

    @Override
    public String toString() {
        return "Node{" + "Content=" + Content + '}';
    }
    
}

public class SinglyLinkedList {

    protected Node head;

    //private int size;
    public Node first() {
        return head;
    }

    public Node last() {
        if(head==null)
            return null;
        Node temp =head;
        while (temp.Next != null) {
            temp = temp.Next;
        }
        return temp;
    }

    public Node next(Node n) {
        return n.Next;
    }

    public Node prev(Node n) {
        if(n==null)
            return null;
        return n.Prev;
    }

    public String remove(Node n) {
        if(!this.isInList(n))
            return "Phan tu khong hop le";
        if(head.Next==null){
            head=null;
            return n.Content;
        }
        if(n.Prev==null){
            head=head.Next;
            head.Prev=null;
            return n.Content;
        }
        n.Prev.Next = n.Next;
        if(n.Next!=null)
            n.Next.Prev = n.Prev;
        return n.Content;
    }

    public Node insertAfter(Node n, String s) {
        if(!this.isInList(n))
            return null;
        Node node = new Node(s);
        node.Prev = n;
        if (n.Next != null) {
            node.Next = n.Next;
            node.Next.Prev = node;
        }
        n.Next = node;
        return node;
    }

    public Node insertBefore(Node n, String s) {
        if(!this.isInList(n))
            return null;
        Node node = new Node(s);
        if (n.Prev == null)
            return insertFirst(s);
        node.Next = n;      
        node.Prev = n.Prev;
        node.Prev.Next = node;

        n.Prev = node;
        return node;
    }

    public Node insertFirst(String s) {
        if(head==null){
            head= new Node(s);
            return head;
        }
        Node node=head;
        node.Prev=new Node(s);
        node.Prev.Next=node;
        head=node.Prev;
        return head;
    }
    public Node insertLast(String s) {
        if(head==null)
            return insertFirst(s);
        Node nose=last();
        nose.Next= new Node(s);
        nose.Next.Prev=nose;
        return last();
    }
    
    public boolean isInList(Node n){
        if(head==null||n==null)// Các Node là null coi như ko thuộc list
            return false;
        Node temp =head;
        while (temp != null) {
            if(temp.Content==n.Content&&temp.Next==n.Next&&temp.Prev==n.Prev)
                return true;
            temp = temp.Next;
        }
        return false;
    }
    
    public boolean isEmpty() {
        return head == null;
    }

    
    public void display() {
       Node temp = head;
        while (temp != null) {
            System.out.print(temp.Content+"; ");
            temp = temp.Next;
        }  
        System.out.println("");
    }

}
