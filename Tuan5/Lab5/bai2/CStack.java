
package Tuan5.Lab5.bai2;

import Tuan5.Lab5.bai1.SinglyLinkedList;


public class CStack {
    private SinglyLinkedList list;

    public CStack() {
        list=new SinglyLinkedList();
    }

    public SinglyLinkedList getList() {
        return list;
    }
    
    public void push(String s){
        list.insertFirst(s);
    }
    public String pop(){
        return list.remove(list.first());
    }
    public void Display(){
        list.display();
    }
    
}
