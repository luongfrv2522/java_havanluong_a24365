
package Tuan5.Lab5.bai3;

import Tuan5.Lab5.bai1.SinglyLinkedList;


public class CQueue {
    private SinglyLinkedList list;

    public CQueue() {
        list=new SinglyLinkedList();
    }

    public SinglyLinkedList getList() {
        return list;
    }
    
    public void enqueue(String s){
        list.insertFirst(s);
    }
    public String dequeue(){
        return list.remove(list.last());
    }
    
    public void Display(){
        list.display();
    }
    
}
