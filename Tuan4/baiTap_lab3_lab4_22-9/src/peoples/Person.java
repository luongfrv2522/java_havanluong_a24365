/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peoples;

/**
 *
 * @author Admin
 */
public class Person {
    private String name;
    Date birthday;

    public Person() {
    }

    public Person(String name, Date birthday) {
        this.name = name;
        this.birthday = birthday;
    }
    
    public String getName(){
        return this.name;
    }

    @Override
    public String toString() {
        return this.name+": "+this.birthday.toString();
    }
    
    
    
    //Test class
//    public static void main(String[] args) {
//        Person per=new Person("Lương Hạ",new Date(26,10,1996));
//        System.out.println(per.toString());
//    }
}
