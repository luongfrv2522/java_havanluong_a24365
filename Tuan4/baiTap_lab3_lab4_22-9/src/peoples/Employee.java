/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peoples;

/**
 *
 * @author Admin
 */
public class Employee extends Person{
    double salary;

    public Employee() {
    }
    
    
    public Employee(String name, Date birthday, double salary) {
        super(name, birthday);
        this.salary = salary;
    }
    @Override
    public String toString() {
        return super.toString()+" : "+this.salary; 
    }


    public String toStringAssiss() {
        return "Asisstant{" + this.toString() + '}';
    }
        
    
    
    
    //Test class
//    public static void main(String[] args) {
//        Employee per=new Employee("Lương Hạ",new Date(26,10,1996),150.5);
//        System.out.println(per.toString());
//    }
}
