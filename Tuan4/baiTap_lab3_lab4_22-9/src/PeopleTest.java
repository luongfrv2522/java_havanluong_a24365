
import peoples.Date;
import peoples.Employee;
import peoples.Person;





public class PeopleTest {

    public static void main(String[] args) {
        /*--1a--*/
        Employee newbie = new Employee("Newbie", new Date(10, 2, 1989), 1000000);

        Manager boss = new Manager("Boss", new Date(23, 2, 1979), 4000000);
        boss.setAssistant(newbie);

        Manager bigBoss = new Manager("Big Boss", new Date(3, 12, 1969), 10000000);
        bigBoss.setAssistant(boss);
//        
//        System.out.println(newbie.toString());
//        System.out.println(boss.toString());
//        System.out.println(bigBoss.toString());

        /*--1b--*/
        Person[] per = new Person[10];
        per[0]=newbie;
        per[1]=boss;
        per[2]=bigBoss;
        int n=3;
        for(int i=0;i<n;i++){
            System.out.println(per[i].toString());
        }
        /*--1c: đã chuyển các file theo yêu cầu--*/
    }
}
