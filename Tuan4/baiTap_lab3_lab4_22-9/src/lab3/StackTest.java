
package lab3;

import java.util.ArrayList;
import java.util.Scanner;




public class StackTest {
    public static void main(String[] args) {
        //Main test
        Stack stack=new Stack();
        //Test Phương thức Push
        stack.push(15);
        stack.push(5);
        stack.push(10);
        stack.push(20);
        stack.push(11);
        stack.push(22);
             //Thứ tự trong stack sẽ là: 20->10->5->15
             //Test với phương thức display
        stack.display();    //display chuẩn, coi như isEmpty chuẩn.
        int n=stack.numOfElement(); //Test phương thức numOfElement()
        System.out.println("Số phần tử của Stack: "+n);
        
        /*--------------------------------------------*/
        //Sau khi Pop
        //pop hết phần tử:
        System.out.println("\n/*------------------------------*/");
        System.out.println("Pop n phần tử trong stack có n phần tử:");
        for (int i = 1; i<=n; i++) {//Pop n phần tử trong stack có n phần tử
            stack.pop();
        }
        stack.display();
        
        
        System.out.println("\n/*------------------------------*/");
        System.out.print("Push lại ");
        stack.push(241);
        stack.push(530);
        stack.push(112);
        stack.push(325);
        stack.push(231);
        stack.push(221);
        stack.display();
        System.out.println("Pop 3 lần (số lần pop ít hơn số phần tử trong stack:");
        for (int i = 1; i<=3; i++) {//Pop 3 lần (số lần pop ít hơn số phần tử trong stack)
            stack.pop();
        }
        stack.display();
        n=stack.numOfElement()+5;
        
        System.out.println("\n/*------------------------------*/");
        System.out.println("Pop n+5 lần (số lần pop nhiều hơn số phần tử trong stack:");
        for (int i = 1; i<=n; i++) {//Pop n+5 lần (số lần pop nhiều hơn số phần tử trong stack)
            stack.pop();
        }
        stack.display();
        /*--------------------------------------------*/
        
        System.out.println("\n/*------------------------------*/");
        System.out.print("Push lại ");
        stack.push(21);
        stack.push(50);
        stack.push(12);
        stack.push(25);
        stack.push(31);
        stack.push(24);
        stack.push(25);
        stack.display();
        
        int u=25;   //tim u trong stack
        
        System.out.println("Test search, "+u+" trong stack: "+stack.search(u));
        
        System.out.print("Test searchMutiIndex ra nhieu vi tri "+u+" trong stack: ");
        ArrayList<Integer> lst=stack.searchMutiIndex(u);
        for (int i = 0; i < lst.size(); i++) {
            System.out.print(lst.get(i)+"; ");
        }
        System.out.println("");
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //Chương trình test 2
        
        
        /*System.out.print("Nhập số lượng phần tử cho stack: ");
        int n = 0;
        try {
            Scanner sc = new Scanner(System.in);
            n = sc.nextInt();
            if (n >= 0) {
                for (int i = 1; i <= n; i++) {
                    System.out.print("Nhập số lần push " + i + " :");
                    int k = sc.nextInt();
                    stack.push(k);
                }
                //Test phương thức display()
                stack.display();//display chuẩn, coi như isEmpty chuẩn.
                System.out.println("Số lượng phần tử: " + stack.numOfElement());//Test phương thức numOfElement()

                System.out.println("Nhập số lần muốn Pop: ");
                //Chú ý: Số lần
                n = sc.nextInt();
                if (n > 0) {
                    boolean cont = false;
                    while (cont) {
                        for (int i = 1; i <= n; i++) {
                            stack.pop();
                        }
                        stack.display();
                    }
                }else
                    System.out.println("Số lần pop phải >0");
            }else{
                System.out.println("Số lần push phải >0");
            }
            
        } catch (Exception e) {
            System.out.println("Nhập sai!");
        }*/
    }
}
