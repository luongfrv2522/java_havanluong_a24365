
package lab3;

//Lop node

import java.util.ArrayList;

class Node {

    int item;
    Node Next;

    public Node() {
        Next = null;
    }

}

public class Stack {

    // Lop Stack
        Node top;
        private int size;

        //Cấu tử không đối số khởi tạo:
        public Stack() {    //khi khởi tạo Stack:
            this.top = null;    //top=Null =>> tự null khi cuối danh sách
            this.size = 0;  //Khi khởi tạo Stack: size=0 vì chưa có phần tử
        }

        //các phương thức:
        
        /*push()- thêm phần tử vào stack*/
        public void push(int item) {
            //Tạo một node mới
            Node node = new Node();
            //Gán giá trị cho item của node
            node.item = item;
            
            if(this.size==0){   //Nếu Stack đang rỗng, chỉ cần cho node hiện tại thành top là được.
                this.top=node;
            }else{  //Nếu stack đã có phần tử:
                node.Next=this.top; //Cho Next của node mới trỏ tới top hiện tại
                this.top=node;  // Sau đó chuyển node mới thành top của stack
            }
            this.size++;
        }
        
        /*Pop() - Lấy top ra khởi ngăn xếp*/
        public int pop(){
            if(this.size==0)    //Size =0 lập tức trả về -1
                return -1;
            size--; //Do nếu =0 thì return -1 và ra khỏi phương thức Pop() do đó không cần else
            int item=this.top.item; //lấy giá trị của top
            this.top=this.top.Next; //Đặt top = phần tử tiếp theo trong stack (Next của top)
            return item;   //Giảm size xuống 1 và trả về giá trị của top
        }
        
        /*search() tìm giá trị item trong stack và trả về số thứ tự, nếu không tìm thấy, hàm  trả về 0*/
        public int search(int item){
            Node temp=top;  //Tạo một Node tạm thời chỉ đến top hiện tại, tránh thao tác khiến top bị dọn mất
            int stt=0;  //stt=0 nếu stack rỗng while ko chạy =>> trả luôn về 0
            while(temp!=null){  //Chạy đến khi temp là rỗng
                    stt++;
                    if(temp.item==item){//Nếu thấy item hiện tại trong stack thì return stt của nó
                        return stt;
                    }
                    temp=temp.Next;//Cho temp trỏ đến node tiếp theo trong stack (Next của temp)
                    //Nếu không phải, tiếp tục tăng stt và vòng while tiếp tục
            }
            return 0;
        }
        
        //searchMutiIndex: Giống như search nhưng sẽ trả về 1 mảng các vị trí của số tìm đc, nếu có nhiều vị trí.
        //Trả về 0 nếu không tìm thấy
        public ArrayList<Integer> searchMutiIndex(int item){
            Node temp=top;  //Tạo một Node tạm thời chỉ đến top hiện tại, tránh thao tác khiến top bị dọn mất
            
            ArrayList<Integer> lsIndex=new ArrayList<>();
            int stt=0;  
            while(temp!=null){  //Chạy đến khi temp là rỗng
                    stt++;
                    if(temp.item==item){//Nếu thấy item hiện tại trong stack thì return stt của nó
                        lsIndex.add(stt);
                    }
                    temp=temp.Next;//Cho temp trỏ đến node tiếp theo trong stack (Next của temp)
                    //Nếu không phải, tiếp tục tăng stt và vòng while tiếp tục
            }
            if(lsIndex.isEmpty()){
                lsIndex.add(0);
            }
            return lsIndex;
        }
        /*Phương thức isEmpty()*/
        public boolean isEmpty(){
            //Size =0 trả về true, nếu không trả về false
            return this.size==0;
        }
        
        public int numOfElement(){
            return this.size;
        }
        
        public void display(){
            if(isEmpty()){  //Main test, khi display chạy đúng coi như isEmpty đúng
                System.out.println("Stack rỗng!"); 
            }else{
                Node temp=top;  //Tạo một Node tạm thời chỉ đến top hiện tại, tránh thao tác khiến top bị dọn mất
                System.out.print("Các phần tử trong Stack: ");
                while(temp!=null){  //Chạy đến khi node là rỗng
                    if(temp.Next!=null) //Nếu là phần tử cuối cùng (Next của nó là null) thì không cần in ra "->"
                        System.out.print(temp.item + "->");
                    else
                        System.out.print(temp.item);
                    temp=temp.Next; //Cho temp hiện tại trỏ đến node tiếp theo trong stack 
                    //Có thể cho tem temp=temp.Next vì bản thân Next cũng là 1 Node
                }
                System.out.println("");
            }
        }


}
