/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bai2_lab4;

/**
 *
 * @author Admin
 */
public class Animal {
    String name;

    public Animal() {
        this.name="no name";
    }

    public Animal(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    
    public void sayHello(){
        System.out.println("Well… I don’t know what to say.");
    }
    public void selfIntroduce(){
        System.out.println("My name is "+ this.name+". I am a "+this.getClass().getSimpleName()); //Lấy ra tên class
        //getClass() sẽ lấy class tương ứng của lớp con và getSimpleName: lấy ra tên của class đó.
        //Như vậy không cần viết thêm hàm này ở Cat và Cow
    }
}
