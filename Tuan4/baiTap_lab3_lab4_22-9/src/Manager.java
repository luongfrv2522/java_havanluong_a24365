
import peoples.Date;
import peoples.Employee;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author Admin
 */
public class Manager extends Employee{
    Employee assistant;

    public Manager() {
    }

    public Manager(String name, Date birthday, double salary) {
        super(name, birthday, salary);
 
    }
    
    public Manager(String name, Date birthday, double salary, Employee assistant) {
        super(name, birthday, salary);
        this.assistant=assistant;
    }
    
    public void setAssistant(Employee assistant){
        this.assistant=assistant;
    }

    @Override
    public String toString() {
        return super.toString()+" : "+this.assistant.toStringAssiss(); 
    }
    
    
    
    
    //Test class
//    public static void main(String[] args) {
//        Manager per=new Manager("Lương Hạ",new Date(26,10,1996),150.5,new Employee("Hạ",new Date(6,11,1992),15000));
//        System.out.println(per.toString());
//    }
}
