
import java.util.Scanner;

public class MyDate {
    int date;
    int mouth;
    int year;

    public MyDate() {
    }

    public MyDate(int date, int mouth, int year) {
        this.date = date;
        this.mouth = mouth;
        this.year = year;
    }
    
    
    //////
    public static MyDate KhoiTao() {
        return new MyDate();
    }
    public static void KhoiTao(MyDate md){
        md=new MyDate();
    }
    
    public void NhapXuat(){
        Scanner sc=new Scanner(System.in);
        try{
            System.out.println("Nhap ngay: ");
            date=sc.nextInt();
            
            System.out.println("Nhap thang: ");
            mouth=sc.nextInt();
            
            System.out.println("Nhap nam: ");
            year=sc.nextInt();
            int maxDay=31;
            
            if(date<=31&&date>=1 && mouth<=12&&mouth>=1 &&year>0){
                if(mouth==2){
                    if(ktNamNhuan(year)){
                        maxDay=29;
                    }else{
                        maxDay=28;
                    }
                }
                if(mouth==4||mouth==6||mouth==9||mouth==11){
                    maxDay=30;
                }
                if(date>maxDay){
                    System.out.println("Thang "+mouth+" chi co "+ maxDay + " ngay!");
                }else{
                    System.out.println("date/mouth/year: "+date+"/"+mouth+"/"+year);
                }
            }else{
                System.out.println("Chi Xet: 0<Ngay<32   0<mouth<13  0<year");
            }
        }catch(Exception ex){
            System.out.println("ngay/thang/nam phai la so nguyen duong");
        }
    }
    
    public static boolean ktNamNhuan(int nam){
        if((nam%4==0 && nam%100!=0) || nam%400==0)
                return true;
        return false;
    }
    //Test
    public static void main(String[] args) {
        MyDate md;
        md=new MyDate();
        
        //Phuong thuc khong doi so
        /*md=KhoiTao();*/
        
        //Phuong thuc Tham so truyen vao co kieu la class
        /*md = null;
        KhoiTao(md);
        md.NhapXuat();*/
        
        //System.out.println(ktNamNhuan(2016));
    }
}
