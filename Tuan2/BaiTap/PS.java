
public class PS {
    float tuso;
    float mauso;

    PS() {
        tuso = 0;
        mauso = 1;
    }

    PS(float a) {
        tuso = a;
        mauso = 1;
    }

    PS(float a, float b) {
        tuso = a;
        mauso = b;
    }
    
    public PS CongPS(PS Ps){
        float kqts=tuso*Ps.mauso+Ps.tuso*mauso;
	float kqms=mauso*Ps.mauso;
	return new PS(kqts,kqms).RutGonPS();
    }
    public PS TruPS(PS Ps){
        float kqts=tuso*Ps.mauso-Ps.tuso*mauso;
	float kqms=mauso*Ps.mauso;
	return new PS(kqts,kqms).RutGonPS();
    }
    public PS NhanPS(PS Ps){
        float kqts=tuso*Ps.tuso;
	float kqms=mauso*Ps.mauso;
	return new PS(kqts,kqms).RutGonPS();
    }
    public PS ChiaPS(PS Ps){
        float kqts=tuso*Ps.mauso;
	float kqms=mauso*Ps.tuso;
	return new PS(kqts,kqms).RutGonPS();
    }
    
    public PS RutGonPS(){
        int a=0,i=(int)tuso;
	if(tuso>mauso)
	{
		a=(int)tuso;
		tuso=mauso;
		mauso=(float)a;
	}
	for(;i>=0;i--)
	{
		if(0==(int)tuso%i&&0==(int)mauso%i)
		{break;}
	}
	if(a!=0)
	{
		mauso=tuso/i;
		tuso=(float)a/i;
		
	}
	else{
	tuso=tuso/i;
	mauso=mauso/i;}
        return this;
    }
    
    //Test:
    public static void main(String[] args) {
        PS ps0=new PS(2,4);
        PS ps1=new PS(1,4);
        System.out.println("Phan so thu 1: "+ps0.tuso+"/"+ps0.mauso);
        System.out.println("Phan so thu 2: "+ps1.tuso+"/"+ps1.mauso);
        
        PS ps=ps0.CongPS(ps1);
        System.out.println("Cong: "+ps.tuso+"/"+ps.mauso);
        
        ps=ps0.TruPS(ps1);
        System.out.println("Tru: "+ps.tuso+"/"+ps.mauso);
        
        ps=ps0.NhanPS(ps1);
        System.out.println("Nhan: "+ps.tuso+"/"+ps.mauso);
        
        ps=ps0.ChiaPS(ps1);
        System.out.println("Chia: "+ps.tuso+"/"+ps.mauso);
    }
}
