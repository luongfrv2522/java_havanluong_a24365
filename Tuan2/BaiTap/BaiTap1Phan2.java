
public class BaiTap1Phan2 {
    //Tim chuoi s1 co nam trong chuoi s2 hay khong? (Co phan biet chu hoa, chu thuong)
    //Dung: tra ve vi tri bat dau cua s1 trong s2
    //Sai: tra ve gia tri null
    public static String substr(String s1,String s2){
        char[] a=s1.toCharArray();
        char[] b=s2.toCharArray();
        
        for(int i=0;i<b.length;i++){
            
            if(b[i]==a[0]){
                int j=0;
                int k=i;
                for(j=0;j<a.length;j++,k++){
                    if(b[k]!=a[j])
                        break;
                }
                if(j==a.length){
                        return String.valueOf(i+1);
                }
            }
        }
        return null;
    }
    

    
    public static void main(String[] args) {
        String a1="hell";
        String a2="8888hello";
        System.out.println("Chuoi s1:"+ a1+"\n"+"Chuoi s2:"+a2);
        System.out.println("Vi tri bat dau cua s1 trong s2: "+substr(a1,a2));
       
    }

}
