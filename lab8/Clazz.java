package lab8;

import java.util.LinkedList;

public class Clazz {

    private static Clazz clazzDuyNhat = null;
    private static String tenLop;
    private static LinkedList<Person> dsHSGV;

    private Clazz() {
    }

    public static Clazz classDuyNhat() {
        if (clazzDuyNhat == null) {
            clazzDuyNhat = new Clazz();
            dsHSGV = new LinkedList<Person>();
        }
        return clazzDuyNhat;
    }

    public void setTenLop(String tenLop) {
        Clazz.tenLop = tenLop;
    }

    public void addDsHSGV(Person p) throws NullPointerException {
        dsHSGV.add(p);
    }

    public String getTenLop() {
        return tenLop;
    }

    public static void setDsHSGV(LinkedList<Person> ds) {
        dsHSGV = ds;
    }

    public LinkedList<Person> getDsHSGV() {
        return dsHSGV;
    }

    public void DisplaydsHSGV() {
        dsHSGV.forEach((x) -> {
            System.out.println(x);
        });
    }

    public static void main(String[] args) {
        try {
            /*Clazz cl1 = Clazz.classDuyNhat();

            cl1.setTenLop("12a5");
            System.out.println("cl1:" + cl1.getTenLop());
            System.out.println("");
        
            Clazz cl2 = Clazz.classDuyNhat();
            cl2.setTenLop("12a6");
            System.out.println("cl1: " + cl1.getTenLop());
            System.out.println("cl2:" + cl2.getTenLop());
            System.out.println("");
            
            
            cl1.DisplaydsHSGV();
             */

            Clazz clPart5 = Clazz.classDuyNhat();
            PersonFactory personFactory = new PersonFactory();

            for (int i = 0; i < 10; i++) {
                clPart5.addDsHSGV(personFactory.createPerson("Student"));
            }
            clPart5.addDsHSGV(personFactory.createPerson("Teacher"));
            int i = 1;
            for (Person p : clPart5.getDsHSGV()) {

                System.out.print("Person thu " + (i++) + ": ");
                p.work();
            }

        } catch (NullPointerException e) {
            System.out.println(e);
        } catch(Exception e){
            System.out.println(e);
        }
    }
}
