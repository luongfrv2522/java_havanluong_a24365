
package lab8;

import java.util.Date;




public class Person {
    private String Ho;
    private String Ten;
    private Date NgaySinh;
    private String LopHoc;

    public Person() {
        NgaySinh=new Date();
    }

    public void setHo(String Ho) {
        this.Ho = Ho;
    }

    public void setTen(String Ten) {
        this.Ten = Ten;
    }

    public void setNgaySinh(Date NgaySinh) {
        this.NgaySinh = NgaySinh;
    }

    public void setLopHoc(String LopHoc) {
        this.LopHoc = LopHoc;
    }

    public String getHo() {
        return Ho;
    }

    public String getTen() {
        return Ten;
    }

    public Date getNgaySinh() {
        return NgaySinh;
    }

    public String getLopHoc() {
        return LopHoc;
    }
    
    public void work(){
        System.out.println( "Khong co viec.");
    }
}
