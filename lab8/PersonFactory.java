
package lab8;


public class PersonFactory {
    
    public Person createPerson(String name) throws NullPointerException{
        Person aPerson=null;
        if(name.equals("Student")){
            aPerson = new Student();
        }else if(name.equals("Teacher")){
            aPerson = new Teacher();
        }
        
        return aPerson;
        
    }
}
